//
//  Model.m
//  MVVM_Example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import "MVVM_model.h"
#import <UIKit/UIKit.h>

@implementation MVVM_model


#pragma mark - Public methods -

- (NSArray*) arrayOfBackgroundColors
{
    return [NSArray arrayWithObjects: [UIColor whiteColor],
                                      [UIColor orangeColor],
                                      [UIColor yellowColor],
                                      [UIColor greenColor], nil];
}


@end
