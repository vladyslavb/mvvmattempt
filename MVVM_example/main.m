//
//  main.m
//  MVVM_example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
