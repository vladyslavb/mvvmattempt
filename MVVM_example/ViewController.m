//
//  ViewController.m
//  MVVM_example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import "ViewController.h"
#import "MVVM_viewManager.h"

@interface ViewController ()

//properties
@property (weak, nonatomic) IBOutlet UITableView* tableView;
@property (weak, nonatomic) IBOutlet UIImageView* imageView;

@end

@implementation ViewController


#pragma mark - Initialization -

- (id) awakeAfterUsingCoder: (NSCoder*) aDecoder
{
    self = [super initWithNibName: @"ViewController"
                           bundle: nil]; // replaces the initial non-xib-storyboard view w/ the correct xib-based view

    if (self)
    {
        //
    }
    
    return self;
}

- (id) initWithViewManager: (MVVM_viewManager*) viewManager
{
    self = [super initWithNibName: @"ViewController"
                           bundle: nil];
    
    if (self)
    {
        __weak MVVM_viewManager* weakViewManager = viewManager;
        
        [self setViewManager: weakViewManager];
    }
    
    return self;
}


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    if (!_viewManager)
    {
        _viewManager = [[MVVM_viewManager alloc] initWithRootViewController: self];
    }
    
    NSString* rightButtonTitle = @"Next";
    
   if (_myViewControllerType == kThirdViewController)
   {
        rightButtonTitle = @"Done";
   }
    
    UIBarButtonItem* nextButton = [[UIBarButtonItem alloc] initWithTitle: rightButtonTitle
                                                                   style: UIBarButtonItemStylePlain
                                                                  target: self action: @selector(handleNextButton)];

    self.navigationItem.rightBarButtonItem = nextButton;
    
    [_imageView setHidden: !_showImageView];
    
    //Added to remove divider lines on cells that are empty
    UIView *footerView = [[UIView alloc] init];
    
    self.tableView.tableFooterView = footerView;
    
    //Set the views background color with the value passed from the view manager
    self.view.backgroundColor = self.backgroundColor;
    
    
}


#pragma mark - UITableViewDelegate

- (CGFloat)   tableView: (UITableView*) tableView
heightForRowAtIndexPath: (NSIndexPath*) indexPath
{
    NSDictionary* curRowDict;
    
    //Gets the dictionary with all the setup values for the current row
    if(self.tableSectionDataArray)
    {
        curRowDict = [self getDictionaryForRow: indexPath];
    }
    
    //If a title object is set in this rows dictionary use the value to set the text for the cell
    if([curRowDict objectForKey: kRowHeight])
    {
        return [[curRowDict objectForKey: kRowHeight] integerValue];
    }
    else
    {
        return kStandardTableRowHeight;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    [_tableView setHidden: self.tableSectionDataArray.count == 0];
        
    //Returns the number of sections in tableSectionDataArray
    return [self.tableSectionDataArray count];
}

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
    NSArray* curSectionArray;
    
    //Unpack the current section array of tableSectionDataArray to see how many row objects are in this section
    if (self.tableSectionDataArray)
    {
        if (self.tableSectionDataArray.count > section)
        {
            curSectionArray = [[NSArray alloc]initWithArray: self.tableSectionDataArray[section]];
        }
    }
    
    return [curSectionArray count];//Regular cell data style
}


- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    UITableViewCell* simpleCell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                                         reuseIdentifier: @"simpleCell"];
    
    NSDictionary* curRowDict;

    //Gets the dictionary with all the setup values for the current row
    if(self.tableSectionDataArray)
    {
        curRowDict = [self getDictionaryForRow:indexPath];
    }
    
    //If a title object is set in this rows dictionary use the value to set the text for the cell
    if([curRowDict objectForKey: kRowTitle])
    {
        simpleCell.textLabel.text = [curRowDict objectForKey: kRowTitle];
    }
    
    //If a color object is set in this rows dictionary use the value to set the background color for the cell
    if([curRowDict objectForKey: kRowColor])
    {
        simpleCell.backgroundColor = [curRowDict objectForKey: kRowColor];
    }

    
    return simpleCell;
}


#pragma mark - User Action

- (void)      tableView: (UITableView*) tableView
didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];
    
    [_viewManager handleRowSelectedAction: indexPath.row
                                   inSection: indexPath.section
                       forViewControllerType: _myViewControllerType];
}

#pragma mark - button handlers


//Passes button action back to the view manager to find out what to do next
- (void) handleNextButton
{
    [_viewManager handleButtonAction: self forViewControllerType: _myViewControllerType];
}


#pragma mark - Utility

//This utility function just unpacks the tableSectionData array and returns the dictionary with the values needed for the current row
- (NSDictionary*) getDictionaryForRow: (NSIndexPath*) indexPath
{
    NSDictionary* curRowDict;
    NSArray*      curSectionArray;
    
    if (self.tableSectionDataArray.count > indexPath.section)
    {
        curSectionArray = [[NSArray alloc] initWithArray: self.tableSectionDataArray[indexPath.section]];
        
        if(curSectionArray.count > indexPath.row)
        {
            curRowDict = [[NSDictionary alloc]initWithDictionary: curSectionArray[indexPath.row]];
        }
    }
    
    return curRowDict;
}
       

@end
