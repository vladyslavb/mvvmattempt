//
//  MVVM_viewManager.m
//  MVVM_Example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

#import "MVVM_model.h"
#import "MVVM_viewManager.h"

@interface MVVM_viewManager ()

@property (weak, nonatomic)   ViewController* rootViewController;
@property (strong, nonatomic) MVVM_model*     dataModel;

@end

@implementation MVVM_viewManager


#pragma mark -

- (id) initWithRootViewController: (ViewController*) rootViewController
{
    self = [MVVM_viewManager new];
    
    if (self) {
        
        _rootViewController = rootViewController;
        
        _dataModel = [MVVM_model new];
        
        [_rootViewController setViewManager: self];

        [self setupKRootViewController: rootViewController];
        
    }
    
    return self;
}

- (ViewController*) getNextViewController: (ViewControllerType) whichViewController
{
    ViewController* viewController = [[ViewController alloc] initWithViewManager: self];
    
    [viewController setMyViewControllerType: whichViewController];
    
    return viewController;
}

- (void) presentViewController: (ViewControllerType) whichViewController
{
    ViewController* viewController = [self getNextViewController: whichViewController];
    
    [self setupViewController: whichViewController
               viewController: viewController];
    
    if (whichViewController == kRootViewController)
    {
        [_rootViewController.navigationController popToRootViewControllerAnimated: YES];

    }
    else
    {
        [_rootViewController.navigationController pushViewController: viewController
                                                            animated: YES];
    }

}


#pragma mark - button action handler

- (void) handleButtonAction: (id)                 sender
      forViewControllerType: (ViewControllerType) currentViewController
{
    switch (currentViewController) {
            
        case kRootViewController:
            
            [self presentViewController: kFirstViewController];
            
            break;
        
        case kFirstViewController:
            
            [self presentViewController: kSecondViewController];
            
            break;
            
         
        case kSecondViewController:
            
            [self presentViewController: kThirdViewController];

            break;
        
        case kThirdViewController:
            
            [self presentViewController: kRootViewController];
            
            break;
            
        default:
            break;
    }
}


#pragma mark - tableview selection handler

- (void) handleRowSelectedAction: (NSInteger) row
                       inSection: (NSInteger) section
           forViewControllerType: (ViewControllerType) currentViewController
{
    NSString* viewControllerName;
    NSString* selectedCell = [NSString stringWithFormat: @"Row selected at section %li, row %li",
                              (long) section,
                              (long) row];

    switch (currentViewController)
    {
        case kRootViewController:
            
            viewControllerName = kRootTitle;
            
            break;
            
        case kFirstViewController:
            
            viewControllerName = kFirstViewTitle;
            
            break;
            
            
        case kSecondViewController:
            
            viewControllerName = kSecondViewTitle;
            
            break;
            
        case kThirdViewController:
            
            viewControllerName = kThirdViewTitle;
            
            break;
            
        default:
            break;
    }
    
    UIAlertView* notice = [[UIAlertView alloc] initWithTitle: viewControllerName
                                                     message: selectedCell
                                                    delegate: self
                                           cancelButtonTitle: @"Ok"
                                           otherButtonTitles: nil];
    
    [notice show];
    
    
}


#pragma mark - setup methods

- (void) setupViewController: (ViewControllerType) whichViewController
               viewController: (ViewController*)    viewController
{
    switch (whichViewController)
    {
        case kRootViewController:
            
            [self setupKRootViewController: viewController];
            
            break;
            
        case kFirstViewController:
            
            [self setup_kFirstViewController: viewController];
            
            break;
        
        case kSecondViewController:
            
            [self setup_kSecondViewController: viewController];
            
            break;
            
        case kThirdViewController:
            
            [self setup_kThirdViewController: viewController];
            
            break;
            
        default:
            break;
    }
    
}


- (void) setupKRootViewController: (ViewController*) viewController
{
    [viewController setTitle: kRootTitle];
    
    [viewController setBackgroundColor: [_dataModel arrayOfBackgroundColors][kRootViewController]];
    
    viewController.tableSectionDataArray = [[NSMutableArray alloc] initWithArray:
                                            @[//Array of sections
                                              @[//section 0
                                                  @{kRowTitle: @"Easily change",
                                                    kRowColor: [UIColor colorWithRed: .9
                                                                               green: 0
                                                                                blue: 0
                                                                               alpha: .5]
                                                    },//row1
                                                  @{kRowTitle: @"the text & color in"},//row2
                                                  @{kRowTitle: @"each cell in the",
                                                    kRowColor: [UIColor colorWithRed: 0
                                                                               green: 0
                                                                                blue: .9
                                                                               alpha: .5]
                                                    },//row3
                                                  @{kRowTitle: @"view manager!",
                                                    kRowColor: [UIColor colorWithRed: 0
                                                                               green:.9
                                                                                blue: 0
                                                                               alpha: .5]
                                                    }//row4
                                                  ]
                                              ]];
    
}


- (void) setup_kFirstViewController: (ViewController*) viewController
{
    [viewController setTitle: kFirstViewTitle];
    
    viewController.tableSectionDataArray = [[NSMutableArray alloc] initWithArray:
                                            @[//Array of sections
                                              @[//section 0
                                                  @{kRowTitle  : @"And yet another row...A BIG row!",
                                                    kRowColor  : [UIColor whiteColor],
                                                    kRowHeight : @100
                                                    }
                                                  ]
                                              ]];
    
    [viewController setBackgroundColor: [_dataModel arrayOfBackgroundColors][kFirstViewController]];
    
}

- (void) setup_kSecondViewController: (ViewController*) viewController
{
    [viewController setTitle: kSecondViewTitle];
    
    [viewController setShowImageView: YES];

    [viewController setBackgroundColor: [_dataModel arrayOfBackgroundColors][kSecondViewController]];
}


- (void) setup_kThirdViewController: (ViewController*) viewController
{
    [viewController setTitle: kThirdViewTitle];
    
    UIColor* cellColor = [UIColor colorWithRed: .9
                                         green: .9
                                          blue: .9
                                         alpha: .5];
    
    viewController.tableSectionDataArray = [[NSMutableArray alloc]initWithArray:
                                            @[//Array of sections
                                              @[//section 0
                                                  @{kRowTitle : @"Amazingly easy",
                                                    kRowColor : cellColor
                                                    },//row1
                                                  @{kRowTitle : @"Crazy Simple",
                                                    kRowColor : cellColor},//row2
                                                  @{kRowTitle : @"Thin View Controller Classes",
                                                    kRowColor : cellColor
                                                    },//row3
                                                  @{kRowTitle : @"Less Defects",
                                                    kRowColor : cellColor
                                                    }//row4
                                                  ]
                                              ]];
    
    [viewController setBackgroundColor: [_dataModel arrayOfBackgroundColors][kThirdViewController]];
    
}


@end
