//
//  MVVM_constants.h
//  MVVM_Example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#ifndef MVVM_Example_MVVM_constants_h
#define MVVM_Example_MVVM_constants_h

typedef enum {
    kRootViewController,
    kFirstViewController,
    kSecondViewController,
    kThirdViewController
} ViewControllerType;

/*
 
 How To Add a New View Controller in 5 Easy Steps!
 
 1. Add a new ViewControllerType enum (above)
 
 2. Add a view title static string (below)
 
 3. Add a setup_ method in the view manager
 
 4. Add a call to your setup_ method in the setupViewController: method in the view manager
 
 5. Add a call to presentViewController: in the handleButtonAction: method in the view manager
 
*/

static NSString* kRootTitle                 = @"Home View Controller";
static NSString* kFirstViewTitle            = @"Orange View Controller";
static NSString* kSecondViewTitle           = @"Yellow View Controller";
static NSString* kThirdViewTitle            = @"Green View Controller";

static const NSString* kRowTitle            = @"rowTitle";
static const NSString* kRowColor            = @"rowColor";
static const NSString* kRowHeight           = @"rowHeight";

static const int kStandardTableRowHeight    = 50;

#endif
