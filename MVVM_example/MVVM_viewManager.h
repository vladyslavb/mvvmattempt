//
//  MVVM_viewManager.h
//  MVVM_Example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import <Foundation/Foundation.h>
#import "MVVM_constants.h"

@class UIViewController;

@interface MVVM_viewManager : NSObject

- (id) initWithRootViewController: (UIViewController*) rootViewController;

- (void) handleButtonAction: (id)                 sender
      forViewControllerType: (ViewControllerType) whichViewController;

- (void) handleRowSelectedAction: (NSInteger)          row
                       inSection: (NSInteger)          section
           forViewControllerType: (ViewControllerType) currentViewController;

@end
