//
//  ViewController.h
//  MVVM_example
//
//  Created by Vladyslav Bedro on 10/17/18.

#import <UIKit/UIKit.h>
#import "MVVM_constants.h"

@class MVVM_viewManager;

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

//properties
@property (strong, nonatomic) MVVM_viewManager* viewManager; // то есть ViewModel

@property (assign, nonatomic) ViewControllerType myViewControllerType;

@property (strong, nonatomic) NSMutableArray* tableSectionDataArray; // array that holds arrays of section data. Each section array holds an array of dictionaries, which hold the data to setup each row.

@property (strong, nonatomic) UIColor* backgroundColor;

@property (assign, nonatomic) BOOL showImageView;

- (id) initWithViewManager: (MVVM_viewManager*) viewManager;

@end

