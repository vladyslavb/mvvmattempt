//
//  AppDelegate.h
//  MVVM_example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

