//
//  Model.h
//  MVVM_Example
//
//  Created by Vladyslav Bedro on 10/17/18.
 
//

#import <Foundation/Foundation.h>

@interface MVVM_model : NSObject

@property (strong, nonatomic) NSArray* arrayOfBackgroundColors;

@end
